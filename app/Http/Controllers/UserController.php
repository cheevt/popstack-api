<?php

namespace App\Http\Controllers;

use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;
use Tymon\JWTAuth\Exceptions\JWTException;
use Tymon\JWTAuth\Facades\JWTAuth;
use App\User;
use App\Role;

class UserController extends Controller {

    public function register(Request $request) {


        $this->validate($request, [
            'first_name' => 'required',
            'last_name' => 'required',
            'email' => 'required|email|unique:users',
            'password' => 'required|confirmed|min:6'
        ]);
        //dd($request);

        $user = new User();
        $user->first_name = $request->first_name;
        $user->last_name = $request->last_name;
        $user->avatar = $request->avatar;
        $user->role_id = $request->role_id;
        $user->email = $request->email;
        $user->password = Hash::make($request->password);
        //$user->is_verified = false;
        $user->save();

        return response()->json(compact('user'));
    }

    public function login(Request $request) {
        // grab credentials from the request
        $credentials = $request->only(['email', 'password']);

        try {
            if (!$token = JWTAuth::attempt($credentials)) {
                return response()->json(['error' => 'invalid_credentials'], 401);
            }
        } catch (JWTException $e) {
            return response()->json(['error' => 'could_not_create_token'], 500);
        }

        //dd($token);

        $user = Auth::user();
        $user_id = $user->id;
        $user_role_id = $user->role_id;

        if($user_role_id !== 1) {
            return response()->json(compact('token', 'user_id'))->header('Authorization', $token);
        }

        return response()->json(compact('token', 'user_id', 'user_role_id'))->header('Authorization', $token);
    }

    public function refreshToken() {
        $token = JWTAuth::getToken();
        $newToken = JWTAuth::refresh($token);

        return response()->json([
            "status" => "success",
            "code" => 200,
            'data' =>
                compact('newToken'),
            'messages' => ['Token refreshed!'],
        ]);
    }

    public function user(Request $request)
    {
        $user = User::find(Auth::user()->id);

        return response([
                'status' => 'success',
                'data' => $user
            ]);
    }

    public function refresh()
    {
        return response([
            'status' => 'success'
        ]);
    }

    public function getUserById($user_id) {
        return User::with('role')->find($user_id);
    }

    public function getUsers() {
        $users = User::with('role')->get();
        
        return response()->json(compact('users'), 200);
    }

    public function edit(Request $request, $user_id) {

        $user = User::find($user_id);
        $user->fill($request->all());
        $user->save();

        return response()->json(compact('user'), 200);
    }

    public function delete($user_id) {
        $user = User::find($user_id);
        $user->delete();

        return response()->json('User deleted!', 200);
    }

}
