<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\JsonResponse;
use App\Http\Controllers\Auth;
use Illuminate\Support\Facades\Validator;
use App\Problem;
use App\Category;

class ProblemController extends Controller {

    public function index() {
        return Problem::with('categories', 'user')->orderBy('id', 'desc')->paginate(6);
    }

    public function getProblemById($problem_id) {
        $problem = Problem::where('id', '=', $problem_id)->with('categories', 'user')->take(1)->get();

        return $problem[0];
    }

    public function getProblemsByUser($user_id) {
        $problems = Problem::where('user_id', $user_id)->get();

        //return $problems;
        return response()->json($problems, 200);
    }

    public function show($slug) {
        $problem = Problem::where('slug', '=', $slug)->with('categories', 'user')->take(1)->get();

        //abort(404, 'Problem doesn\'t exist');

        return $problem[0];
    }

    public function store(Request $request) {
        $this->validate($request, [
            'title' => 'required|string|min:8',
            'text' => 'required',
            'user_id' => 'required'
        ]);

        $problem = new Problem();
        $problem->title = $request->input('title');
        $problem->slug = str_slug($request->input('title'), '-');
        $problem->text = $request->input('text');
        $problem->user_id = $request->input('user_id');

        $problem->save();


        foreach ($request->input('category_ids') as $categoryId) {
            $problem->categories()->attach($categoryId);
        }

        return $problem;
    }

    public function update(Request $request, $problem_id) {
        $problem = Problem::find($problem_id);
        $problem->title = $request->input('title');
        $problem->slug = str_slug($request->input('title'), '-');
        $problem->text = $request->input('text');
        $problem->user_id = $request->input('user_id');
        $problem->save();

        $problem->categories()->detach();

        foreach ($request->input('category_ids') as $categoryId) {
            $problem->categories()->attach($categoryId);
        }

        return response()->json(compact('problem'), 200);
    }

    public function delete($problem_id) {
        $problem = Problem::find($problem_id);
        $problem->delete();

        return response()->json(true, 200);
    }

}
