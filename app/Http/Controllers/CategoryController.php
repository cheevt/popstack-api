<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\JsonResponse;
use App\Http\Controllers\Auth;
use Illuminate\Support\Facades\Validator;
use App\Category;

class CategoryController extends Controller {

    public function index() {
        return Category::with('parent')->get();
    }

    public function store(Request $request) {
        $this->validate($request, [
            'name' => 'required|string|min:4'
        ]);

        $category = new Category();
        $category->name = $request->input('name');
        $category->slug = str_slug($request->input('name'), '-');
        $category->parent_id = $request->input('parent_id');

        $category->save();

        return $category;
    }

    public function update(Request $request, $id) {
        $this->validate($request, [
            'name' => 'required|string|min:4'
        ]);

        $category = Category::find($id);
        $category->name = $request->input('name');
        $category->slug = str_slug($request->input('name'), '-');
        $category->parent_id = $request->input('parent_id');

        $category->save();
        
        $category = Category::where('id', $id)->with('parent')->take(1)->get();
        
        return $category[0];
    }

    public function destroy($id) {
        $category = Category::find($id);
        $category->delete();

        return new JsonResponse(true);
    }

}
