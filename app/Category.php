<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model {

    public function problems() {
        return $this->belongsToMany('App\Problem');
    }

    //each category might have one parent
    public function parent() {
        return $this->belongsTo(static::class, 'parent_id');
    }

    //each category might have multiple children
    public function children() {
        return $this->hasMany(static::class, 'parent_id')->orderBy('name', 'asc');
    }
}
