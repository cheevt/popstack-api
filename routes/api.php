<?php

use Illuminate\Http\Request;

/*
  |--------------------------------------------------------------------------
  | API Routes
  |--------------------------------------------------------------------------
  |
  | Here is where you can register API routes for your application. These
  | routes are loaded by the RouteServiceProvider within a group which
  | is assigned the "api" middleware group. Enjoy building your API!
  |
 */

/* Route::middleware('auth:api')->get('/user', function (Request $request) {
  return $request->user();
  }); */

Route::post('/register', 'UserController@register');
Route::post('/login', 'UserController@login');
Route::middleware('jwt.refresh')->get('/retoken', 'UserController@refreshToken');
Route::middleware('jwt.auth')->get('/user', 'UserController@user');
Route::middleware('jwt')->get('/users', 'UserController@getUsers');
Route::middleware('jwt')->get('/users/{id}', 'UserController@getUserById');
Route::middleware('jwt')->put('/users/{id}', 'UserController@edit');
Route::middleware('jwt')->delete('/users/{id}', 'UserController@delete');
Route::get('/categories', 'CategoryController@index');
Route::middleware('jwt')->post('/categories', 'CategoryController@store');
Route::middleware('jwt')->put('/categories/{id}', 'CategoryController@update');
Route::middleware('jwt')->delete('/categories/{id}', 'CategoryController@destroy');
Route::get('/problems', 'ProblemController@index');
Route::get('/problems/{id}', 'ProblemController@getProblemById')->where('id', '[0-9]+');
Route::get('/problems/{slug}', 'ProblemController@show');
Route::get('/problems/user/{user_id}', 'ProblemController@getProblemsByUser');
Route::middleware('jwt')->post('/problems', 'ProblemController@store');
Route::middleware('jwt')->put('/problems/{id}', 'ProblemController@update');
Route::middleware('jwt')->delete('/problems/{id}', 'ProblemController@delete');

